public class Uczestnik  {
    private static int liczbaKobiet = 0;
    private String imie;
    
    private static final String EXCEPTION_NAME = "kuba";
    
    Uczestnik(String imie) {
        this.imie = imie;
        if (imie.substring(imie.length() - 1).equals("a") && !EXCEPTION_NAME.equals(imie.toLowerCase())) liczbaKobiet++;
    }
    
    public static int getLiczbaKobiet() { return liczbaKobiet; }
    
    }
}
